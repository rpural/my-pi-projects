#! /usr/bin/python

import time
import random
import thread
import re
import os
import RPi.GPIO as GPIO
from Tkinter import *

go1 = 11 # red LED
go2 = 13 # red LED
go3 = 15 # red LED
go4 = 18 # green LED via transistor

gi1 = 16 # photo sensor as switch
gi2 = 22 # switch

glight = 12 # photo sensor as scale 

mindelay = .01
maxdelay = .25

runtime = 15

go1state = 0
go2state = 0
go3state = 0
go4state = 0

timerlock = thread.allocate_lock()

light = 0

temp = 70.0
ds18s20path = "/sys/bus/w1/devices/10-00080234080e/w1_slave"

def blinker(gp, final):
        while timerlock.locked(): 
                state = random.choice((GPIO.HIGH, GPIO.LOW,))
                wait = random.random() * maxdelay + mindelay
                GPIO.output(gp, state)
                time.sleep(wait)
        else:
                GPIO.output(gp, final)

def flash(gp, final):
        while timerlock.locked():
                GPIO.output(gp, (not final))
                time.sleep(.25)
                GPIO.output(gp, final)
                time.sleep(.25)

def runner():
        while timerlock.locked():
                GPIO.output(go1, GPIO.LOW)
                time.sleep(.15)
                GPIO.output(go2, GPIO.LOW)
                GPIO.output(go1, GPIO.HIGH)
                time.sleep(.15)
                GPIO.output(go3, GPIO.LOW)
                GPIO.output(go2, GPIO.HIGH)
                time.sleep(.15)
                GPIO.output(go3, GPIO.HIGH)
                time.sleep(.5)

def RCtime (RCpin):
        reading = 0
        GPIO.setup(RCpin, GPIO.OUT)
        GPIO.output(RCpin, GPIO.LOW)
        time.sleep(0.05)
 
        GPIO.setup(RCpin, GPIO.IN)
        # This takes about 1 millisecond per loop cycle
        while (GPIO.input(RCpin) == GPIO.LOW):
                reading += 1
                if reading >= 65000:
                        break
        return reading

def SenseLight():
	global light
	while True:
		light = RCtime(glight)
		time.sleep(3)

def SenseTemp():
	global temp
	while True:
		ds18s20 = open(ds18s20path, 'r')
		check = ds18s20.readline()
		if check.find("YES") != -1:
			templine = ds18s20.readline()
			match = re.search('t=(\d+)', templine)
			ttemp = float(match.group(1)) / 1000.0
			temp = (ttemp * (9.0 / 5.0)) + 32.0
		ds18s20.close()
		time.sleep(10)

class App:

    def __init__(self, master):
	master.title("Breadboard")
	frame = Frame(master)
	frame.pack()

	self.quit = Button(frame, text="Quit", command=frame.quit)
	self.quit.grid(row=7,column=0)

	self.led1button = Button(frame, text="LED 1", activeforeground="red", command=self.LED1)
	self.led1button.grid(row=0,column=0)

	self.led2button = Button(frame, text="LED 2", activeforeground="red", command=self.LED2)
	self.led2button.grid(row=0,column=1)

	self.led3button = Button(frame, text="LED 3", activeforeground="red", command=self.LED3)
	self.led3button.grid(row=0,column=2)

	self.led4button = Button(frame, text="LED 4", activeforeground="green", command=self.LED4)
	self.led4button.grid(row=0,column=3)

	self.randombutton = Button(frame, text="Random Display", command=self.randdisp)
	self.randombutton.grid(row=1,column=0,columnspan=3, sticky=E+W)

	self.seqbutton = Button(frame, text="Sequential Display", command=self.seqdisp)
	self.seqbutton.grid(row=2,column=0,columnspan=3, sticky=E+W)

	self.lightvalue = StringVar()
	self.lightvalue.set("1")
	self.ltlabel = Label(frame, text="Light:")
	self.ltlabel.grid(row=3,column=0, sticky=W)
	self.ltvalue = Label(frame, width=6, textvariable=self.lightvalue)
	self.ltvalue.grid(row=3,column=1, sticky=E)
	self.ltdisp = Canvas(frame, height=10, width=10, relief=RIDGE, borderwidth=5)
	self.ltdisp.grid(row=3, column=2, columnspan=2, sticky=N+S+W+E)

	self.ltvalue.after(5000,self.updatelight)

	self.tempvalue = StringVar()
	self.tempvalue.set("70.0")
	self.tplabel = Label(frame, text="Temp:")
	self.tplabel.grid(row=4,column=0, sticky=W)
	self.tpvalue = Label(frame, width=8, textvariable=self.tempvalue)
	self.tpvalue.grid(row=4,column=1, sticky=E)

	self.tpvalue.after(5000,self.updatetemp)

	self.swlabel = Label(frame, text="Switches:")
	self.swlabel.grid(row=5, column=0, sticky=E)

	self.sw1 = Label(frame, bitmap="info")
	self.sw1.grid(row=5, column=1)
	self.sw1.after(2000,self.cksw1)

	self.sw2 = Label(frame, bitmap="info")
	self.sw2.grid(row=5, column=2)
	self.sw2.after(2000,self.cksw2)

    def cksw1(self):
	if GPIO.input(gi1):
		self.sw1.configure(bitmap="warning")
	else:
		self.sw1.configure(bitmap="info")
	self.sw1.after(2000,self.cksw1)

    def cksw2(self):
	if not GPIO.input(gi2):
		self.sw2.configure(bitmap="hourglass")
	else:
		self.sw2.configure(bitmap="info")
	self.sw2.after(2000,self.cksw2)

    def updatelight(self):
	self.lightvalue.set(str(light))
	self.ltvalue.after(5000, self.updatelight)
	adjust = 4096 - int(light / 65000.0 * 4095.0)
	lthex = hex(adjust)
	lthex = "00" + lthex[2:]
	lthex = lthex[-3:]
	self.ltdisp.configure(background="#"+lthex+lthex+lthex)

    def updatetemp(self):
	self.tempvalue.set(str(temp))
	self.tpvalue.after(5000, self.updatetemp)

    def LED1(self):
	global go1state
	if go1state == 0:
		go1state = 1
		GPIO.output(go1, GPIO.LOW)
	else:
		go1state = 0
		GPIO.output(go1, GPIO.HIGH)

    def LED2(self):
        global go2state
        if go2state == 0:
                go2state = 1
                GPIO.output(go2, GPIO.LOW)
        else:   
                go2state = 0
                GPIO.output(go2, GPIO.HIGH)

    def LED3(self):
        global go3state
        if go3state == 0:
                go3state = 1
                GPIO.output(go3, GPIO.LOW)
        else:   
                go3state = 0
                GPIO.output(go3, GPIO.HIGH)
    
    def LED4(self):
        global go4state
        if go4state == 0:
                go4state = 1
                GPIO.output(go4, GPIO.HIGH)
        else:   
                go4state = 0
                GPIO.output(go4, GPIO.LOW)

    def randdisp(self):
	global go1state, go2state, go3state, go4state
	go1state = 0
	go2state = 0
	go3state = 0
	go4state = 0
	timerlock.acquire()
	thread.start_new_thread(blinker, (go1, True,))
	thread.start_new_thread(blinker, (go2, True,))
	thread.start_new_thread(blinker, (go3, True,))
	time.sleep(runtime)
	timerlock.release()

    def seqdisp(self):
        global go1state, go2state, go3state, go4state
        go1state = 0
        go2state = 0
        go3state = 0
        go4state = 0
	timerlock.acquire()
	thread.start_new_thread(runner, ())
	time.sleep(runtime)
	timerlock.release()


GPIO.setmode(GPIO.BOARD)

GPIO.setup(go1, GPIO.OUT)
GPIO.setup(go2, GPIO.OUT)
GPIO.setup(go3, GPIO.OUT)
GPIO.setup(go4, GPIO.OUT)

GPIO.setup(gi1, GPIO.IN)
GPIO.setup(gi2, GPIO.IN)

GPIO.output(go1, GPIO.HIGH)
GPIO.output(go2, GPIO.HIGH)
GPIO.output(go3, GPIO.HIGH)
GPIO.output(go4, GPIO.LOW)

thread.start_new_thread(SenseLight, ())
thread.start_new_thread(SenseTemp, ())

root = Tk()

app = App(root)

root.mainloop()

