#!/usr/bin/python

import time
import random
import thread
import RPi.GPIO as GPIO

go1 = 11
go2 = 13
go3 = 15

gi1 = 16
gi2 = 22

mindelay = .01
maxdelay = .25

runtime = 15

timerlock = thread.allocate_lock()

def blinker(gp, final):
	while timerlock.locked(): 
		state = random.choice((True, False,))
		wait = random.random() * maxdelay + mindelay
		GPIO.output(gp, state)
		time.sleep(wait)
	else:
		GPIO.output(gp, final)

def flash(gp, final):
	while timerlock.locked():
		GPIO.output(gp, (not final))
		time.sleep(.25)
		GPIO.output(gp, final)
		time.sleep(.25)

def runner():
	while timerlock.locked():
		GPIO.output(go1, False)
		time.sleep(.15)
		GPIO.output(go2, False)
		GPIO.output(go1, True)
		time.sleep(.15)
		GPIO.output(go3, False)
		GPIO.output(go2, True)
		time.sleep(.15)
		GPIO.output(go3, True)
		time.sleep(.5)

GPIO.setmode(GPIO.BOARD)

GPIO.setup(go1, GPIO.OUT)
GPIO.setup(go2, GPIO.OUT)
GPIO.setup(go3, GPIO.OUT)

GPIO.setup(gi1, GPIO.IN)
GPIO.setup(gi2, GPIO.IN)

GPIO.output(go1, False)
GPIO.output(go2, True)
GPIO.output(go3, True)

timerlock.acquire()
thread.start_new_thread(flash, (go1, True,))
time.sleep(2)
timerlock.release()

while True:
	if GPIO.input(gi1):
		timerlock.acquire()
		thread.start_new_thread(blinker, (go1, True,))
		thread.start_new_thread(blinker, (go2, True,))
		thread.start_new_thread(blinker, (go3, True,))
		time.sleep(runtime)
		timerlock.release()
	
	if not GPIO.input(gi2):
		timerlock.acquire()
		thread.start_new_thread(runner, ())
		time.sleep(runtime)
		timerlock.release()
	
	time.sleep(1)

